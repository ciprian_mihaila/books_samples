package jcip.buildingblocks.blockingqueue.desktopsearch;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * starts several crawlers and indexers, each in their own thread. 
 * As written, the consumer threads never exit, which prevents the program from terminating
 * @author Ciprian
 *
 */
public class DesktopSearch {

	public static final int BOUND = 10;
	public static final int N_CONSUMERS = 10;

	public static void startIndexing(File[] roots) {
		BlockingQueue<File> queue = new LinkedBlockingQueue<File>(BOUND);
		FileFilter filter = new FileFilter() {
			public boolean accept(File file) {
				return true;
			}
		};
		for (File root : roots) {
			new Thread(new FileCrawler(queue, filter, root)).start();
		}

		for (int i = 0; i < N_CONSUMERS; i++) {
			new Thread(new Indexer(queue)).start();
		}
	}
}
