package jcip.buildingblocks.blockingqueue.desktopsearch;

import java.io.File;
import java.util.concurrent.BlockingQueue;

/**
 * Producer and Consumer Tasks in a Desktop Search Application. 
 * 
 * FileCrawler shows a producer task that searches a file hierarchy for 
 * files meeting an indexing criterion and puts their names on the work queue
 * 
 * @author Ciprian
 *
 */
public class FileCrawler implements Runnable {
	private final BlockingQueue<File> fileQueue;
	private final FileFilter fileFilter;
	private final File root;

	public FileCrawler(BlockingQueue<File> fileQueue, FileFilter fileFilter, File root) {
		this.fileQueue = fileQueue;
		this.fileFilter = fileFilter;
		this.root = root;
	}

	public void run() {
		try {
			crawl(root);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	private void crawl(File root) throws InterruptedException {
		File[] entries = root.listFiles(fileFilter);
		if (entries != null) {
			for (File entry : entries) {
				if (entry.isDirectory()) {
					crawl(entry);
				} else if (!alreadyIndexed(entry)) {
					fileQueue.put(entry);
				}
			}
		}
	}

	private boolean alreadyIndexed(File entry) {
		return false;
	}
}
