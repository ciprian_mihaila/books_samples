package jcip.threadsafety.locking.intrinsiclocks;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.GenericServlet;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import jcip.annotations.GuardedBy;
import jcip.annotations.ThreadSafe;

/**
 * provides a balance between 
 * 		simplicity (synchronizing the entire method) and
 * 		concurrency (synchronizing the shortest possible code paths)
 * @author Ciprian
 *
 */
@ThreadSafe
public class CachedFactorizer extends GenericServlet implements Servlet {

	private static final long serialVersionUID = 1L;

	@GuardedBy("this")
	private BigInteger lastNumber;

	@GuardedBy("this")
	private BigInteger[] lastFactors;

	@GuardedBy("this")
	private long hits;

	@GuardedBy("this")
	private long cacheHits;

	public synchronized long getHits() {
		return hits;
	}

	public synchronized double getCacheHitRatio() {
		return (double) cacheHits / (double) hits;
	}

	@Override
	public synchronized void service(ServletRequest req, ServletResponse resp) throws ServletException, IOException {
		BigInteger i = extractFromRequest(req);
		BigInteger[] factors = null;
		synchronized (this) {
			++hits;
			if (i.equals(lastNumber)) {
				++cacheHits;
				factors = lastFactors.clone();
			}
		}
		if (factors == null) {
			factors = factor(i);
			synchronized (this) {
				lastNumber = i;
				lastFactors = factors.clone();
			}
		}
		encodeIntoResponse(resp, factors);
	}

	void encodeIntoResponse(ServletResponse resp, BigInteger[] factors) {
		System.out.println("encoded into response");
	}

	private BigInteger[] factor(BigInteger i) {
		return new BigInteger[] { i };
	}

	private BigInteger extractFromRequest(ServletRequest req) {
		return new BigInteger("1");
	}

}
