package chapter2.item2.builderforhierarchies;

import static chapter2.item2.builderforhierarchies.NyPizza.Size.SMALL;
import static chapter2.item2.builderforhierarchies.Pizza.Topping.HAM;
import static chapter2.item2.builderforhierarchies.Pizza.Topping.ONION;
import static chapter2.item2.builderforhierarchies.Pizza.Topping.SAUSAGE;

public class Test {
	public static void main(String[] args) {
		NyPizza pizza = new NyPizza.Builder(SMALL).addTopping(SAUSAGE).addTopping(ONION).build();
		Calzone calzone = new Calzone.Builder().addTopping(HAM).sauceInside().build();

		System.out.println(pizza.toString());
		System.out.println(calzone.toString());
	}
}
