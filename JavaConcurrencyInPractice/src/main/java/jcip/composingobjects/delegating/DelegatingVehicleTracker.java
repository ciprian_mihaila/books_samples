package jcip.composingobjects.delegating;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import jcip.annotations.ThreadSafe;

/**
 * DelegatingVehicleTracker does not use any explicit synchronization; 
 * all access to state is managed by ConcurrentHashMap, and all 
 * the keys and values of the Map are immutable. 
 * @author Ciprian
 *
 */
@ThreadSafe
public class DelegatingVehicleTracker {
	private final ConcurrentMap<String, Point> locations;
	private final Map<String, Point> unmodifiableMap;

	public DelegatingVehicleTracker(Map<String, Point> points) {
		locations = new ConcurrentHashMap<String, Point>(points);
		unmodifiableMap = Collections.unmodifiableMap(locations);
	}

	public Map<String, Point> getLocations() {
		return unmodifiableMap;
	}

	public Point getLocation(String id) {
		return locations.get(id);
	}

	public void setLocation(String id, int x, int y) {
		if (locations.replace(id, new Point(x, y)) == null)
			throw new IllegalArgumentException("invalid vehicle name: " + id);
	}

	/**
	 * Returning a Static Copy of the Location Set Instead of a "Live" One. 
	 * @return
	 */
	public Map<String, Point> getLocationsStatic() {
		return Collections.unmodifiableMap(new HashMap<String, Point>(locations));
	}
}

// @Immutable
class Point {
	public final int x, y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
