package jcip.composingobjects.addingfunctionality;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jcip.annotations.NotThreadSafe;

/**
 * Non thread safe Attempt to Implement Put if absent. Don't Do this. 
 * 
 * The problem is that it synchronizes on the wrong lock. 
 * Whatever lock the List uses to guard its state, it sure 
 * isn't the lock on the ListHelper
 * @author Ciprian
 *
 * @param <E>
 */
@NotThreadSafe
public class ListHelper<E> {
	public List<E> list = Collections.synchronizedList(new ArrayList<E>());

	public synchronized boolean putIfAbsent(E x) {
		boolean absent = !list.contains(x);
		if (absent)
			list.add(x);
		return absent;
	}
}
