package chapter2.item5;

import java.util.ArrayList;
import java.util.List;

/**
 * Inappropriate use of static utility - inflexible & untestable!
 * @author Ciprian
 *
 */
public class SpellChecker1 {
	private static final Lexicon dictionary = new Lexicon();

	// Noninstantiable
	private SpellChecker1() {
	}

	public static boolean isValid(String word) {
		return false;
	}

	public static List<String> suggestions(String typo) {
		return new ArrayList<>();
	}

}

class Lexicon {
}
