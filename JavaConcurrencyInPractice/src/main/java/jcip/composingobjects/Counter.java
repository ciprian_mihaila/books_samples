package jcip.composingobjects;

import jcip.annotations.GuardedBy;
import jcip.annotations.ThreadSafe;

/**
 * Simple Thread safe Counter Using the Java Monitor Pattern.
 * @author Ciprian
 *
 */
@ThreadSafe
public class Counter {

	@GuardedBy("this")
	private long value = 0;

	public synchronized long getValue() {
		return value;
	}

	public synchronized long increment() {
		if (value == Long.MAX_VALUE) {
			throw new IllegalStateException("counter overflow");
		}
		return ++value;
	}
}
