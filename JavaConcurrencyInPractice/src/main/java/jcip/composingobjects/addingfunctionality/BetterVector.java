package jcip.composingobjects.addingfunctionality;

import java.util.Vector;

import jcip.annotations.ThreadSafe;

/**
 * Extending Vector to have a Put if absent Method. 
 * 
 * @author Ciprian
 *
 * @param <E>
 */
@ThreadSafe
public class BetterVector<E> extends Vector<E> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public synchronized boolean putIfAbsent(E x) {
		boolean absent = !contains(x);
		if (absent) {
			add(x);
		}
		return absent;
	}
}