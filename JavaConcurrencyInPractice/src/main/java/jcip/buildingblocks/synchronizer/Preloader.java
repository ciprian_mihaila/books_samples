package jcip.buildingblocks.synchronizer;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Using FutureTask to Preload Data that is Needed Later
 * 
 * uses FutureTask to perform an expensive computation whose results are needed later; 
 * by starting the computation early, you reduce the time you would have to wait later 
 * when you actually need the results
 * 
 * @author Ciprian
 *
 */
public class Preloader {
	private final FutureTask<ProductInfo> future = new FutureTask<ProductInfo>(new Callable<ProductInfo>() {
		public ProductInfo call() throws DataLoadException {
			return loadProductInfo();
		}
	});
	private final Thread thread = new Thread(future);

	public void start() {
		thread.start();
	}

	public ProductInfo get() throws DataLoadException, InterruptedException {
		try {
			return future.get();
		} catch (ExecutionException e) {
			Throwable cause = e.getCause();
			if (cause instanceof DataLoadException) {
				throw (DataLoadException) cause;
			} else {
				throw launderThrowable(cause);
			}
		}
	}

	private ProductInfo loadProductInfo() {
		System.out.println("load product info");
		return new ProductInfo();
	}

	/** If the Throwable is an Error, throw it; if it is a
	* RuntimeException return it, otherwise throw IllegalStateException
	*/
	public static RuntimeException launderThrowable(Throwable t) {
		if (t instanceof RuntimeException) {
			return (RuntimeException) t;
		} else if (t instanceof Error) {
			throw (Error) t;
		} else {
			throw new IllegalStateException("Not unchecked", t);
		}
	}
}

class ProductInfo {
}

class DataLoadException extends Exception {

	private static final long serialVersionUID = 1L;

}