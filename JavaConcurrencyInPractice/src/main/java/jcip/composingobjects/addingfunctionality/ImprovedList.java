package jcip.composingobjects.addingfunctionality;

import java.util.List;

import jcip.annotations.ThreadSafe;

/**
 * Implementing Put if absent Using Composition.
 * @author Ciprian
 *
 * @param <T>
 */
@ThreadSafe
public abstract class ImprovedList<T> implements List<T> {
	private final List<T> list;

	public ImprovedList(List<T> list) {
		this.list = list;
	}

	public synchronized boolean putIfAbsent(T x) {
		boolean contains = list.contains(x);

		if (contains) {
			list.add(x);
		}

		return !contains;
	}

	public synchronized void clear() {
		list.clear();
	}
	// ... similarly delegate other List methods
}
