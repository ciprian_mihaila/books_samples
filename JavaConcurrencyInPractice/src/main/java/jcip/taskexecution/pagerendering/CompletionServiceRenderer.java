package jcip.taskexecution.pagerendering;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import jcip.common.ThrowableUtils;

/**
 * Using CompletionService to Render Page Elements as they Become Available
 * 
 * We can use a CompletionService to improve the performance of the page renderer in two ways: 
 * shorter total runtime and improved responsiveness. We can create a separate task for downloading 
 * each image and execute them in a thread pool, turning the sequential download into a parallel one: 
 * this reduces the amount of time to download all the images. And by fetching results from the 
 * CompletionService and rendering each image as soon as it is available, we can give 
 * the user a more dynamic and responsive user interface
 * 
 * @author Ciprian
 *
 */
public class CompletionServiceRenderer {
	private final ExecutorService executor;

	CompletionServiceRenderer(ExecutorService executor) {
		this.executor = executor;
	}

	void renderPage(CharSequence source) {
		final List<ImageInfo> info = scanForImageInfo(source);

		CompletionService<ImageData> completionService = new ExecutorCompletionService<ImageData>(executor);

		for (final ImageInfo imageInfo : info) {
			completionService.submit(new Callable<ImageData>() {
				public ImageData call() {
					return imageInfo.downloadImage();
				}
			});
		}

		renderText(source);

		try {
			for (int t = 0, n = info.size(); t < n; t++) {
				Future<ImageData> f = completionService.take();
				ImageData imageData = f.get();
				renderImage(imageData);
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			throw ThrowableUtils.launderThrowable(e.getCause());
		}
	}

	private void renderText(CharSequence source) {
	}

	private void renderImage(ImageData data) {
	}

	private List<ImageInfo> scanForImageInfo(CharSequence source) {
		return null;
	}
}
