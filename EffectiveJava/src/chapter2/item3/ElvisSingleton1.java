package chapter2.item3;

/**
 * Singleton with public final field
 * @author Ciprian
 *
 */
public class ElvisSingleton1 {
	public static final ElvisSingleton1 INSTANCE = new ElvisSingleton1();

	private ElvisSingleton1() {
	}

	public void leaveTheBuilding() {
	}
}
