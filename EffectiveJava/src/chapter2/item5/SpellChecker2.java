package chapter2.item5;

import java.util.ArrayList;
import java.util.List;

/**
 * Inappropriate use of singleton - inflexible & untestable!
 * 
 * @author Ciprian
 *
 */
public class SpellChecker2 {
	private static final Lexicon dictionary = new Lexicon();

	public static SpellChecker2 INSTANCE = new SpellChecker2();

	private SpellChecker2() {
	}

	public static boolean isValid(String word) {
		return false;
	}

	public static List<String> suggestions(String typo) {
		return new ArrayList<>();
	}

}
