package jcip.threadsafety.atomicity;

import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.GenericServlet;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import jcip.annotations.ThreadSafe;

/**
 * use existing thread safe objects, like AtomicLong, to manage class's state
 * @author Ciprian
 *
 */
@ThreadSafe
public class CountingFactorizer extends GenericServlet implements Servlet {

	private static final long serialVersionUID = 1L;

	private final AtomicLong count = new AtomicLong(0);

	public long getCount() {
		return count.get();
	}

	@Override
	public void service(ServletRequest req, ServletResponse resp) throws ServletException, IOException {
		BigInteger i = extractFromRequest(req);
		BigInteger[] factors = factor(i);
		count.incrementAndGet();
		encodeIntoResponse(resp, factors);
	}

	void encodeIntoResponse(ServletResponse resp, BigInteger[] factors) {
		System.out.println("encoded into response");
	}

	private BigInteger[] factor(BigInteger i) {
		return new BigInteger[] { i };
	}

	private BigInteger extractFromRequest(ServletRequest req) {
		return new BigInteger("1");
	}

}
