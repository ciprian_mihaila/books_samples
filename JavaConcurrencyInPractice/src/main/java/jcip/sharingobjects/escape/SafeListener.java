package jcip.sharingobjects.escape;

/**
 * Using a Factory Method to Prevent the this Reference from Escaping During Construction. 
 * @author Ciprian
 *
 */
public class SafeListener {
	private final EventListener listener;

	private SafeListener() {
		listener = new EventListener() {
			public void onEvent(Event e) {
				doSomething(e);
			}
		};
	}

	void doSomething(Event e) {
	}

	public static SafeListener newInstance(EventSource source) {
		SafeListener safe = new SafeListener();
		source.registerListener(safe.listener);
		return safe;
	}
}
