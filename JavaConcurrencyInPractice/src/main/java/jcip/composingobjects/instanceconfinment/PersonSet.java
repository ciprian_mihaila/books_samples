package jcip.composingobjects.instanceconfinment;

import java.util.HashSet;
import java.util.Set;

import jcip.annotations.GuardedBy;
import jcip.annotations.ThreadSafe;

/**
 * Using Confinement to Ensure Thread Safety
 *  illustrates how confinement and locking can work together to make a 
 *  class thread safe even when its component state variables are not.
 * @author Ciprian
 *
 */
@ThreadSafe
public class PersonSet {

	@GuardedBy("this")
	private final Set<Person> mySet = new HashSet<Person>();

	public synchronized void addPerson(Person p) {
		mySet.add(p);
	}

	public synchronized boolean containsPerson(Person p) {
		return mySet.contains(p);
	}

}

class Person {

}
