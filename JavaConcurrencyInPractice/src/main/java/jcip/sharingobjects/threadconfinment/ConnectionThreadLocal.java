package jcip.sharingobjects.threadconfinment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Using ThreadLocal to Ensure thread confinement
 * 
 * ThreadLocal, which allows you to associate a per thread value with 
 * a value holding object. Thread-Local provides get and set accessor 
 * methods that maintain a separate copy of  the  value  for  each  thread  
 * that  uses  it,  so  a  get  returns  the  most  recent  value  passed  
 * to  set  from  the  currently executing thread
 * 
 * @author Ciprian
 *
 */
public class ConnectionThreadLocal {

	public static final String DB_URL = "jdbc:mysql://localhost/mydatabase";

	private static ThreadLocal<Connection> connectionHolder = new ThreadLocal<Connection>() {
		public Connection initialValue() {
			try {
				return DriverManager.getConnection(DB_URL);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return null;
		}
	};

	public static Connection getConnection() {
		return connectionHolder.get();
	}
}
