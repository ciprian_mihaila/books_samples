package chapter2.item1;

public class StaticFactoriesExample {

	public static Boolean valueOf(boolean b) {
		return b ? Boolean.TRUE : Boolean.FALSE;
	}

	public static void main(String[] args) {
		System.out.println(StaticFactoriesExample.valueOf(true));
		System.out.println(StaticFactoriesExample.valueOf(false));
	}
}
