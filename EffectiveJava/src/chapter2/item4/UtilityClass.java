package chapter2.item4;

/**
 * Noninstantiable utility class
 * 
 * @author Ciprian
 *
 */
public class UtilityClass {

	// Suppress default constructor for noninstantiability
	private UtilityClass() {
		throw new AssertionError();
	}

}
