package jcip.threadsafety.locking.reentrancy;

public class Widget {
	public synchronized void doSomething() {
		System.out.println("do something widget");
	}
}
