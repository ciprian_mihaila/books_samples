package jcip.buildingblocks.synchronizedcollections;

import java.util.Vector;

/**
 * The problem of unreliable iteration can again be addressed by
 * client side locking, at some additional cost to scalability. 
 * 
 * @author Ciprian
 *
 */
public class IterationWithClienSideLocking {
	private Vector<Integer> vector;

	public void iterate() {
		synchronized (vector) {
			for (int i = 0; i < vector.size(); i++) {
				doSomething(vector.get(i));
			}
		}
	}

	private void doSomething(Integer i) {
	}
}
