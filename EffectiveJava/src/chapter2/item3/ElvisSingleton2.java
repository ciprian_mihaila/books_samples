package chapter2.item3;

/**
 * Singleton with static factory
 * 
 * @author Ciprian
 *
 */
public class ElvisSingleton2 {
	private static final ElvisSingleton2 INSTANCE = new ElvisSingleton2();

	private ElvisSingleton2() {

	}

	public static ElvisSingleton2 getInstance() {
		return INSTANCE;
	}

	public void leaveTheBuilding() {
	}
}
