package jcip.buildingblocks.synchronizedcollections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Iterating a List with an Iterator. 
 * 
 * illustrates iterating a collection with the for each loop syntax. 
 * Internally, javac generates code that uses an Iterator, repeatedly 
 * calling hasNext and next to iterate the List. Just as with iterating the Vector, 
 * the way to prevent ConcurrentModificationException is to hold the collection 
 * lock for the duration of the iteration.
 * 
 * The iterators returned by the synchronized collections are not designed to deal 
 * with concurrent modification, and they are fail fast meaning that if they detect 
 * that the collection has changed since iteration began, they throw the unchecked 
 * ConcurrentModificationException
 * 
 * @author Ciprian
 *
 */
public class ConcurrentModificationExcep {

	private List<Widget> widgetList = Collections.synchronizedList(new ArrayList<Widget>());

	// May throw ConcurrentModificationException
	public void iterate() {
		for (Widget w : widgetList) {
			doSomething(w);
		}
	}

	private void doSomething(Widget w) {
	}

}

class Widget {
}
