package jcip.sharingobjects.visibility;

/**
 * sharing variables without synchronization
 * the absence of synchronization, the compiler, processor, and runtime can do some 
 * downright weird things to the order in which operations appear to execute
 * @author Ciprian
 *
 */
public class NoVisibility {
	private static boolean ready;
	private static int number;

	private static class ReaderThread extends Thread {
		public void run() {
			while (!ready)
				Thread.yield();
			System.out.println(number);
		}
	}

	public static void main(String[] args) {
		new ReaderThread().start();
		number = 42;
		ready = true;
	}
}
