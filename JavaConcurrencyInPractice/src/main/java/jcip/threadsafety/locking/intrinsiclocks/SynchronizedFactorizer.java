package jcip.threadsafety.locking.intrinsiclocks;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.GenericServlet;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import jcip.annotations.GuardedBy;
import jcip.annotations.ThreadSafe;

/**
 * servlet that caches last result, but with unacceptably poor concurrency (performance problem)s
 * @author Ciprian
 *
 */
@ThreadSafe
public class SynchronizedFactorizer extends GenericServlet implements Servlet {

	private static final long serialVersionUID = 1L;

	@GuardedBy("this")
	private BigInteger lastNumber;

	@GuardedBy("this")
	private BigInteger[] lastFactors;

	@Override
	public synchronized void service(ServletRequest req, ServletResponse resp) throws ServletException, IOException {
		BigInteger i = extractFromRequest(req);
		if (i.equals(lastNumber)) {
			encodeIntoResponse(resp, lastFactors);
		} else {
			BigInteger[] factors = factor(i);
			lastNumber = i;
			lastFactors = factors;
			encodeIntoResponse(resp, factors);
		}
	}

	void encodeIntoResponse(ServletResponse resp, BigInteger[] factors) {
		System.out.println("encoded into response");
	}

	private BigInteger[] factor(BigInteger i) {
		return new BigInteger[] { i };
	}

	private BigInteger extractFromRequest(ServletRequest req) {
		return new BigInteger("1");
	}

}
