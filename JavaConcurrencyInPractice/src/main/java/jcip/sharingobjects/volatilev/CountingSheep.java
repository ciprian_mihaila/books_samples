package jcip.sharingobjects.volatilev;

/**
 * typical use of volatile variables: checking a status flag to 
 * determine when to exit a loop.
 * 
 * asleep flag must be volatile. otherwise, the thread might not 
 * notice when asleep has been set by another thread
 * 
 * @author Ciprian
 *
 */
public class CountingSheep {
	volatile boolean asleep;

	void tryToSleep() {
		while (!asleep) {
			countSomeSheep();
		}
	}

	void countSomeSheep() {
		// One, two, three...
	}
}
