package jcip.threadsafety.atomicity;

import jcip.annotations.NotThreadSafe;

/**
 * check then act
 * 
 * @author Ciprian
 *
 */
@NotThreadSafe
public class LazyInitRace {
	private ExpensiveObject instance = null;

	public ExpensiveObject getInstance() {
		if (instance == null) {
			instance = new ExpensiveObject();
		}
		return instance;
	}
}

class ExpensiveObject {

}
