package jcip.taskexecution.pagerendering;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import jcip.common.ThrowableUtils;

/**
 * Waiting for Image Download with Future
 * 
 * Callable and Future can help us express the interaction between these cooperating tasks. 
 * In FutureRenderer we create a Callable to download all the images, and submit it to an ExecutorService. 
 * This returns a Future describing the task's execution; when the main task gets to the point 
 * where it needs the images, it waits for the result by calling Future.get. 
 * If we're lucky, the results will already be ready by the time we ask; otherwise, 
 * at least we got a head start on downloading the images. 
 * 
 * @author Ciprian
 *
 */
public class FutureRenderer {
	private final ExecutorService executor = Executors.newCachedThreadPool();

	void renderPage(CharSequence source) {
		final List<ImageInfo> imageInfos = scanForImageInfo(source);

		Callable<List<ImageData>> task = new Callable<List<ImageData>>() {
			public List<ImageData> call() {
				List<ImageData> result = new ArrayList<ImageData>();
				for (ImageInfo imageInfo : imageInfos) {
					result.add(imageInfo.downloadImage());
				}
				return result;
			}
		};

		Future<List<ImageData>> future = executor.submit(task);
		renderText(source);
		try {
			List<ImageData> imageData = future.get();
			for (ImageData data : imageData) {
				renderImage(data);
			}
		} catch (InterruptedException e) {
			// Re-assert the thread's interrupted status
			Thread.currentThread().interrupt();
			// We don't need the result, so cancel the task too
			future.cancel(true);
		} catch (ExecutionException e) {
			throw ThrowableUtils.launderThrowable(e.getCause());
		}
	}

	private void renderText(CharSequence source) {
	}

	private void renderImage(ImageData data) {
	}

	private List<ImageInfo> scanForImageInfo(CharSequence source) {
		return null;
	}

}
