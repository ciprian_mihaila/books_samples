package jcip.threadsafety.stateless;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.GenericServlet;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import jcip.annotations.ThreadSafe;

/**
 * Stateless objects are always threadsafe.
 * 
 * @author Ciprian
 *
 */
@ThreadSafe
public class StatelessFactorizer extends GenericServlet implements Servlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void service(ServletRequest req, ServletResponse resp) throws ServletException, IOException {
		BigInteger i = extractFromRequest(req);
		BigInteger[] factors = factor(i);
		encodeIntoResponse(resp, factors);
	}

	void encodeIntoResponse(ServletResponse resp, BigInteger[] factors) {
		System.out.println("encoded into response");
	}

	private BigInteger[] factor(BigInteger i) {
		return new BigInteger[] { i };
	}

	private BigInteger extractFromRequest(ServletRequest req) {
		return new BigInteger("1");
	}

}
