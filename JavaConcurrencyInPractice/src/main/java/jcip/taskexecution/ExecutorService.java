package jcip.taskexecution;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/**
 * 
 * Lifecycle Methods in ExecutorService
 * 
 * @author Ciprian
 *
 */
public interface ExecutorService extends Executor {
	void shutdown();

	List<Runnable> shutdownNow();

	boolean isShutdown();

	boolean isTerminated();

	boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException;
	// ... additional convenience methods for task submission
}
