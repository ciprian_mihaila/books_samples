package jcip.buildingblocks.synchronizedcollections;

import java.util.Vector;

/**
 * Compound Actions on Vector Using Client side Locking
 * 
 * The synchronized collection classes guard each method with 
 * the lock on the synchronized collection object itself.  
 * By  acquiring  the  collection  lock  we  can  make  getLast  and  deleteLast  atomic,  
 * ensuring  that  the  size  of  the Vector does not change between calling size and get
 * 
 * @author Ciprian
 *
 */
public class ClientSideLocking {
	public static Object getLast(Vector list) {
		synchronized (list) {
			int lastIndex = list.size() - 1;
			return list.get(lastIndex);
		}
	}

	public static void deleteLast(Vector list) {
		synchronized (list) {
			int lastIndex = list.size() - 1;
			list.remove(lastIndex);
		}
	}
}
