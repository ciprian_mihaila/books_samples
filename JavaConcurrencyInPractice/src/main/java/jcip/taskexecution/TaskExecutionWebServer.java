package jcip.taskexecution;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Web Server Using a Thread Pool
 * 
 * TaskExecutionWebServer, submission of the request handling task 
 * is decoupled from its execution using an Executor, and its 
 * behavior can be changed merely by substituting a 
 * different Executor implementation
 * 
 * @author Ciprian
 *
 */
public class TaskExecutionWebServer {
	private static final int NTHREADS = 100;
	private static final Executor exec = Executors.newFixedThreadPool(NTHREADS);

	public static void main(String[] args) throws IOException {
		ServerSocket socket = new ServerSocket(80);
		while (true) {
			final Socket connection = socket.accept();
			Runnable task = new Runnable() {
				public void run() {
					handleRequest(connection);
				}
			};
			exec.execute(task);
		}
	}

	private static void handleRequest(Socket connection) {
		// handle request...
	}
}
