package jcip.taskexecution.pagerendering;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RunnableFuture;

/**
 * When a task is submitted, it is wrapped with a QueueingFuture, 
 * a subclass of FutureTask that overrides done to place the result on the BlockingQueue
 * 
 * QueueingFuture Class Used By ExecutorCompletionService
 * 
 * @author Ciprian
 *
 * @param <V>
 */
public class QueueingFuture<V> extends FutureTask<V> {
	BlockingQueue<RunnableFuture> completionQueue = new LinkedBlockingQueue<>();

	QueueingFuture(Callable<V> c) {
		super(c);
	}

	QueueingFuture(Runnable t, V r) {
		super(t, r);
	}

	protected void done() {
		completionQueue.add(this);
	}
}
