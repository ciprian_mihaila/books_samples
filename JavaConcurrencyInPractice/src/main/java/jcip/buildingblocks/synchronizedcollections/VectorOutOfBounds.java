package jcip.buildingblocks.synchronizedcollections;

import java.util.Vector;

/**
 * Iteration that may Throw ArrayIndexOutOfBoundsException. 
 * 
 * The risk that the size of the list might change between a call to size and 
 * the corresponding call to get is also present when we iterate through the elements of a Vector
 * 
 * This iteration idiom relies on a leap of faith that other threads will not modify the 
 * Vector between the calls to size and get
 * 
 * Even though the iteration can throw an exception, this doesn't mean Vector isn't thread safe. 
 * The state of the Vector is still valid and the exception is in fact in conformance with its 
 * specification. However, that something as mundane as fetching the last element or iteration 
 * throw an exception is clearly undesirable. 
 * 
 * @author Ciprian
 *
 */
public class VectorOutOfBounds {

	private Vector<Integer> vector;

	public void iterate() {
		for (int i = 0; i < vector.size(); i++) {
			doSomething(vector.get(i));
		}
	}

	private void doSomething(Integer i) {
	}

}
