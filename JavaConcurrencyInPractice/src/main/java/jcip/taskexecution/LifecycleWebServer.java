package jcip.taskexecution;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

import org.omg.CORBA.Request;

/**
 * Web Server with Shutdown Support
 * It can be shut down in two ways: 
 * 		programmatically by calling stop, 
 * 		and through a client request by sending the web server 
 * 		a specially formatted HTTP request
 * 
 * @author Ciprian
 *
 */
public class LifecycleWebServer {
	private static final int NTHREADS = 10;
	private final java.util.concurrent.ExecutorService exec = Executors.newFixedThreadPool(NTHREADS);

	public void start() throws IOException {
		ServerSocket socket = new ServerSocket(80);
		while (!exec.isShutdown()) {
			try {
				final Socket conn = socket.accept();
				exec.execute(new Runnable() {
					public void run() {
						handleRequest(conn);
					}
				});
			} catch (RejectedExecutionException e) {
				if (!exec.isShutdown()) {
					System.out.println("task submission rejected " + e);
				}
			}
		}
	}

	public void stop() {
		exec.shutdown();
	}

	void handleRequest(Socket connection) {
		Request req = readRequest(connection);
		if (isShutdownRequest(req)) {
			stop();
		} else {
			dispatchRequest(req);
		}
	}

	private Request readRequest(Socket s) {
		return null;
	}

	private void dispatchRequest(Request r) {
	}

	private boolean isShutdownRequest(Request r) {
		return false;
	}

}
