package chapter2.item3;

/**
 * Enum singleton - the preferred approach
 * 
 * @author Ciprian
 *
 */
public enum ElvisSingleton4 {
	INSTANCE;

	public void leaveTheBuilding() {
	}
}
