package jcip.buildingblocks.cache;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * Memorizing Wrapper Using FutureTask.
 * 
 * The Memorizer3 implementation is almost perfect: it exhibits very good concurrency 
 * (mostly derived from the excellent concurrency of ConcurrentHashMap), the result 
 * is returned efficiently if it is already known, and if the computation is in progress 
 * by another thread, newly arriving threads wait patiently for the result. It has only 
 * one defect there is still a small window of vulnerability in which two threads might compute the same value
 * 
 * Memorizer3 is vulnerable to this problem because a compound action (put if absent) is 
 * performed on the backing map that cannot be made atomic using locking
 * 
 * @author Ciprian
 *
 * @param <A>
 * @param <V>
 */
public class Memorizer3<A, V> implements Computable<A, V> {

	private final Map<A, Future<V>> cache = new ConcurrentHashMap<A, Future<V>>();
	private final Computable<A, V> c;

	public Memorizer3(Computable<A, V> c) {
		this.c = c;
	}

	public V compute(final A arg) throws InterruptedException {
		Future<V> f = cache.get(arg);
		if (f == null) {
			Callable<V> eval = new Callable<V>() {
				public V call() throws InterruptedException {
					return c.compute(arg);
				}
			};
			FutureTask<V> ft = new FutureTask<V>(eval);
			f = ft;
			cache.put(arg, ft);
			ft.run(); // call to c.compute happens here
		}
		try {
			return f.get();
		} catch (ExecutionException e) {
			throw launderThrowable(e.getCause());
		}
	}

	private RuntimeException launderThrowable(Throwable t) {
		if (t instanceof RuntimeException) {
			return (RuntimeException) t;
		} else if (t instanceof Error) {
			throw (Error) t;
		} else {
			throw new IllegalStateException("Not unchecked", t);
		}
	}

}
