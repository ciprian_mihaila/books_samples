package jcip.buildingblocks.synchronizer;

import java.util.concurrent.CountDownLatch;

/**
 * Using CountDownLatch for Starting and Stopping Threads in Timing Tests
 * 
 * A latch is a synchronizer that can delay the progress of threads until 
 * it reaches its terminal state. A latch acts as a gate: until the latch 
 * reaches the terminal state the gate is closed and no thread can pass, 
 * and in the terminal state the gate opens, allowing all threads to pass
 * 
 * 
 * @author Ciprian
 *
 */
public class TestHarness {
	public long timeTasks(int nThreads, final Runnable task) throws InterruptedException {
		final CountDownLatch startGate = new CountDownLatch(1);
		final CountDownLatch endGate = new CountDownLatch(nThreads);

		for (int i = 0; i < nThreads; i++) {
			Thread t = new Thread() {
				public void run() {
					try {
						startGate.await();
						try {
							task.run();
						} finally {
							endGate.countDown();
						}
					} catch (InterruptedException ignored) {
					}
				}
			};
			t.start();
		}

		long start = System.nanoTime();
		startGate.countDown();
		endGate.await();
		long end = System.nanoTime();
		return end - start;
	}
}
