package chapter2.item5;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Dependency injection provides flexibility and testability
 * @author Ciprian
 *
 */
public class SpellChecker3 {
	private final Lexicon dictionary;

	public SpellChecker3(Lexicon dictionary) {
		this.dictionary = Objects.requireNonNull(dictionary);
	}

	public static boolean isValid(String word) {
		return false;
	}

	public static List<String> suggestions(String typo) {
		return new ArrayList<>();
	}
}
