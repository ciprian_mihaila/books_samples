package jcip.composingobjects.instanceconfinment;

import jcip.annotations.GuardedBy;
import jcip.threadsafety.locking.reentrancy.Widget;

/**
 * Guarding State with a Private Lock
 * @author Ciprian
 *
 */
public class PrivateLock {
	private final Object myLock = new Object();

	@GuardedBy("myLock")
	Widget widget;

	void someMethod() {
		synchronized (myLock) {
			// Access or modify the state of widget
		}
	}
}
