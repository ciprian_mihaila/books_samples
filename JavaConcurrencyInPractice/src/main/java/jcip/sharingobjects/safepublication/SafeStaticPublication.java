package jcip.sharingobjects.safepublication;

/**
 * Static initializers are executed by the JVM at class initialization time; 
 * because of internal synchronization in the JVM, this mechanism is guaranteed 
 * to safely publish any objects initialized in this way
 * 
 * @author Ciprian
 *
 */
public class SafeStaticPublication {
	public static Holder holder = new Holder(42);
}
