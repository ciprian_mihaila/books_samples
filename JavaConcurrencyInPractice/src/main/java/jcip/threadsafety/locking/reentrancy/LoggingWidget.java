package jcip.threadsafety.locking.reentrancy;

/**
 * reentrancy means that locks are acquired per thread rather than per invocation basis
 * @author Ciprian
 *
 */
public class LoggingWidget extends Widget {
	@Override
	public synchronized void doSomething() {
		System.out.println("LoggingWidget calling do something");
		super.doSomething();
	}
}
