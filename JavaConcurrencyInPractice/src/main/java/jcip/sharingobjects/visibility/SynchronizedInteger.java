package jcip.sharingobjects.visibility;

import jcip.annotations.ThreadSafe;

/**
 * thread safe mutable Integer holder
 * @author Ciprian
 *
 */

@ThreadSafe
public class SynchronizedInteger {
	private int value;

	public synchronized int get() {
		return value;
	}

	public synchronized void set(int value) {
		this.value = value;
	}
}
