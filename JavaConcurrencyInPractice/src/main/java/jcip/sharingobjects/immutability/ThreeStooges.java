package jcip.sharingobjects.immutability;

import java.util.HashSet;
import java.util.Set;

import jcip.annotations.Immutable;

/**
 * Immutable Class Built Out of Mutable Underlying Objects. 
 * 
 * @author Ciprian
 *
 */
@Immutable
public class ThreeStooges {
	private final Set<String> stooges = new HashSet<String>();

	public ThreeStooges() {
		stooges.add("Moe");
		stooges.add("Larry");
		stooges.add("Curly");
	}

	public boolean isStooge(String name) {
		return stooges.contains(name);
	}
}
