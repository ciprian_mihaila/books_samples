package jcip.buildingblocks.synchronizedcollections;

import java.util.Vector;

/**
 * Compound Actions on a Vector that may Produce Confusing Results
 * 
 * shows two methods that operate on a Vector, getLast and delete-Last, 
 * both of which are check then act sequences. Each calls size to determine 
 * the size of the array and uses the resulting value to retrieve or remove 
 * the last element
 * 
 * These methods seem harmless, and in a sense they are they can't corrupt 
 * the Vector, no matter how many threads call them simultaneously. 
 * But the caller of these methods might have a different opinion. 
 * If thread A calls getLast on a Vector with ten elements, thread B 
 * calls deleteLast on the same Vector, and the operations are interleaved
 * 
 * @author Ciprian
 *
 */
public class ConfuseResults {
	public static Object getLast(Vector list) {
		int lastIndex = list.size() - 1;
		return list.get(lastIndex);
	}

	public static void deleteLast(Vector list) {
		int lastIndex = list.size() - 1;
		list.remove(lastIndex);
	}
}
