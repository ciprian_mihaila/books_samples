package chapter2.item3;

/**
 * To maintain the singleton guarantee, declare all instance fields
 * transient and provide a readResolve method (Item 89). 
 * Otherwise, each time a serialized instance is deserialized, 
 * a new instance will be create
 * 
 * @author Ciprian
 *
 */
public class ElvisSingleton3 {
	private static final ElvisSingleton3 INSTANCE = new ElvisSingleton3();

	private ElvisSingleton3() {

	}

	public static ElvisSingleton3 getInstance() {
		return INSTANCE;
	}

	public void leaveTheBuilding() {
	}

	/** 
	 * readResolve method to preserve singleton property
	 * 
	 * @return
	 */
	private Object readResolve() {
		// Return the one true Elvis and let the garbage collector
		// take care of the Elvis impersonator.
		return INSTANCE;
	}
}
