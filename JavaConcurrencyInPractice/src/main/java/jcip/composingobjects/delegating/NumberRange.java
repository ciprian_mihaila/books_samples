package jcip.composingobjects.delegating;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Number Range Class that does Not Sufficiently Protect Its Invariants. Don't Do this.
 * 
 * uses  two  AtomicIntegers  to  manage  its  state,  but  imposes  an  additional 
 * constraint that the first number be less than or equal to the second
 * @author Ciprian
 *
 */
public class NumberRange {
	// INVARIANT: lower <= upper
	private final AtomicInteger lower = new AtomicInteger(0);
	private final AtomicInteger upper = new AtomicInteger(0);

	public void setLower(int i) {
		// Warning -- unsafe check-then-act
		if (i > upper.get()) {
			throw new IllegalArgumentException("can't set lower to " + i + " > upper");
		}
		lower.set(i);
	}

	public void setUpper(int i) {
		// Warning -- unsafe check-then-act
		if (i < lower.get()) {
			throw new IllegalArgumentException("can't set upper to " + i + " < lower");
		}
		upper.set(i);
	}

	public boolean isInRange(int i) {
		return (i >= lower.get() && i <= upper.get());
	}
}
