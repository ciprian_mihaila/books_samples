package jcip.taskexecution;

import java.util.concurrent.Executor;

/**
 * Executor that Executes Tasks Synchronously in the Calling Thread
 * 
 * Executor that would make TaskExecutionWebServer behave like the single 
 * threaded version, executing each task synchronously before returning from execute
 * 
 * @author Ciprian
 *
 */
public class WithinThreadExecutor implements Executor {
	public void execute(Runnable r) {
		r.run();
	};
}