package jcip.buildingblocks.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Replacing HashMap with ConcurrentHashMap
 * 
 * Since ConcurrentHashMap is thread safe, there is no need to synchronize when accessing the 
 * backing Map, thus eliminating the serialization induced by synchronizing compute in Memorizer1.
 * 
 * Memorizer2 certainly has better concurrent behavior than Memorizer1: multiple threads can 
 * actually use it concurrently. But it still has some defects as a cache there is a window of 
 * vulnerability in which two threads calling compute at the same time could end up computing the 
 * same value. In the case of memorization, this is merely inefficient the purpose of a cache is 
 * to prevent the same data from being calculated multiple times. For a more general purpose 
 * caching mechanism, it is far worse; for an object cache that is supposed to provide once and only once 
 * initialization, this vulnerability would also pose a safety risk. 
 * 
 * The problem with Memorizer2 is that if one thread starts an expensive computation, 
 * other threads are not aware that the computation is in progress and so may start the same computation
 * 
 * 
 * @author Ciprian
 *
 * @param <A>
 * @param <V>
 */
public class Memorizer2<A, V> implements Computable<A, V> {

	private final Map<A, V> cache = new ConcurrentHashMap<A, V>();
	private final Computable<A, V> c;

	public Memorizer2(Computable<A, V> c) {
		this.c = c;
	}

	public V compute(A arg) throws InterruptedException {
		V result = cache.get(arg);
		if (result == null) {
			result = c.compute(arg);
			cache.put(arg, result);
		}
		return result;
	}

}
