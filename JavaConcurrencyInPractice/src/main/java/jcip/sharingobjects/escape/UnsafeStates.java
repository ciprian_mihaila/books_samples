package jcip.sharingobjects.escape;

/**
 * Allowing internal mutable state to escape. Don't do this.
 * @author Ciprian
 *
 */
public class UnsafeStates {
	private String[] states = new String[] { "AK", "AL" };

	public String[] getStates() {
		return states;
	}
}
