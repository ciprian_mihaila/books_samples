package jcip.buildingblocks.blockingqueue.desktopsearch;

import java.io.File;
import java.util.concurrent.BlockingQueue;

/**
 * shows the consumer task that takes file names from the queue and indexes them.
 * @author Ciprian
 *
 */
public class Indexer implements Runnable {
	private final BlockingQueue<File> queue;

	public Indexer(BlockingQueue<File> queue) {
		this.queue = queue;
	}

	public void run() {
		try {
			while (true) {
				indexFile(queue.take());
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	public void indexFile(File file) {
		System.out.println("indexing file...");
	}
}
