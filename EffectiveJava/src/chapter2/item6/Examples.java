package chapter2.item6;

import java.util.regex.Pattern;

/**
 * Avoid creating unnecessary objects
 * 
 * @author Ciprian
 *
 */
public class Examples {

	public void example1() {
		String s1 = new String("bikini"); // DON'T DO THIS!
		String s2 = "bikini";// The improved version
	}

	public void example2() {
		boolean b1 = Boolean.valueOf("true"); // preferable
		boolean b2 = new Boolean("true"); // not preferable
	}

	// Performance can be greatly improved!
	static boolean isRomanNumeral(String s) {
		return s.matches("^(?=.)M*(C[MD]|D?C{0,3})" + "(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})$");
	}

	// Reusing expensive object for improved performance
	private static class RomanNumerals {
		private static final Pattern ROMAN = Pattern
				.compile("^(?=.)M*(C[MD]|D?C{0,3})" + "(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})$");

		static boolean isRomanNumeral(String s) {
			return ROMAN.matcher(s).matches();
		}
	}

	// Hideously slow! Can you spot the object creation?
	private static long sum() {
		Long sum = 0L;
		for (long i = 0; i <= Integer.MAX_VALUE; i++) {
			sum += i;
		}
		return sum;
	}

	// prefer primitives to boxed primitives, and watch out for unintentional
	// autoboxing
	private static long sum2() {
		long sum = 0L;
		for (long i = 0; i <= Integer.MAX_VALUE; i++)
			sum += i;
		return sum;
	}

}
