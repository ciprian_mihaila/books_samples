package jcip.composingobjects.addingfunctionality;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jcip.annotations.ThreadSafe;

/**
 * Implementing Put if absent with Client side Locking
 * @author Ciprian
 *
 * @param <E>
 */
@ThreadSafe
public class ListHelperSafe<E> {
	public List<E> list = Collections.synchronizedList(new ArrayList<E>());

	public boolean putIfAbsent(E x) {
		synchronized (list) {
			boolean absent = !list.contains(x);
			if (absent) {
				list.add(x);
			}
			return absent;
		}
	}
}