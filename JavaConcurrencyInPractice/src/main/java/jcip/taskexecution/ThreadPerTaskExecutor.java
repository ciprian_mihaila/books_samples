package jcip.taskexecution;

import java.util.concurrent.Executor;

/**
 * Executor that Starts a New Thread for Each Task
 * 
 * @author Ciprian
 *
 */
public class ThreadPerTaskExecutor implements Executor {
	public void execute(Runnable r) {
		new Thread(r).start();
	};
}
