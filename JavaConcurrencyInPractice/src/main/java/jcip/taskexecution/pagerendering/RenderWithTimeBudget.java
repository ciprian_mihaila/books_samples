package jcip.taskexecution.pagerendering;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

/**
 * Fetching an Advertisement with a Time Budget
 * 
 * shows a typical application of a timed Future.get. It generates a composite web page 
 * that contains the requested content plus an advertisement fetched from an ad server. 
 * It submits the ad fetching task to an executor, computes the rest of the page content, 
 * and then waits for the ad until its time budget runs out. If the get times out, it 
 * cancels the ad fetching task and uses a default advertisement instead
 * @author Ciprian
 *
 */
public class RenderWithTimeBudget {
	private static final Ad DEFAULT_AD = new Ad();
	private static final long TIME_BUDGET = 1000;
	private static final ExecutorService exec = Executors.newCachedThreadPool();

	private Page renderPageBody() {
		return null;
	}

	Page renderPageWithAd() throws InterruptedException {
		long endNanos = System.nanoTime() + TIME_BUDGET;
		Future<Ad> f = exec.submit(new FetchAdTask());
		// Render the page while waiting for the ad
		Page page = renderPageBody();
		Ad ad;
		try {
			// Only wait for the remaining time budget
			long timeLeft = endNanos - System.nanoTime();
			ad = f.get(timeLeft, NANOSECONDS);
		} catch (ExecutionException e) {
			ad = DEFAULT_AD;
		} catch (TimeoutException e) {
			ad = DEFAULT_AD;
			f.cancel(true);
		}
		page.setAd(ad);
		return page;
	}
}

class FetchAdTask implements Callable<Ad> {
	public Ad call() {
		return new Ad();
	}
}

class Ad {
}

interface Page {
	public void setAd(Ad ad);
}
