package jcip.sharingobjects.visibility;

import jcip.annotations.NotThreadSafe;

/**
 * non thread safe mutable Integer holder
 * @author Ciprian
 *
 */
@NotThreadSafe
public class MutableInteger {
	private int value;

	public int get() {
		return value;
	}

	public void set(int value) {
		this.value = value;
	}
}
