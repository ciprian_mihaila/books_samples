package jcip.sharingobjects.safepublication;

/**
 * Publishing an Object without Adequate Synchronization. Don't Do this
 * 
 * Holder is published using the unsafe publication, and a thread other than 
 * the publishing thread were to call assertSanity, it could throw AssertionError! 
 * 
 * @author Ciprian
 *
 */
public class ImproperPublication {
	public Holder holder;

	// safe publication
	public static Holder holder2 = new Holder(42);

	public void initialize() {
		holder = new Holder(42);
	}
}

class Holder {
	private int n;

	public Holder(int n) {
		this.n = n;
	}

	public void assertSanity() {
		if (n != n) {
			throw new AssertionError("This statement is false.");
		}

	}
}
