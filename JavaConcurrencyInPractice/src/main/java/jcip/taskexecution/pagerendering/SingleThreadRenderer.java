package jcip.taskexecution.pagerendering;

import java.util.ArrayList;
import java.util.List;

/**
 * Rendering Page Elements Sequentially
 * 
 * @author Ciprian
 *
 */
public class SingleThreadRenderer {
	void renderPage(CharSequence source) {
		renderText(source);
		List<ImageData> imageData = new ArrayList<ImageData>();
		for (ImageInfo imageInfo : scanForImageInfo(source)) {
			imageData.add(imageInfo.downloadImage());
		}
		for (ImageData data : imageData) {
			renderImage(data);
		}
	}

	private void renderText(CharSequence source) {
	}

	private void renderImage(ImageData data) {
	}

	private List<ImageInfo> scanForImageInfo(CharSequence source) {
		return null;
	}
}

interface ImageData {
}

interface ImageInfo {
	ImageData downloadImage();
}