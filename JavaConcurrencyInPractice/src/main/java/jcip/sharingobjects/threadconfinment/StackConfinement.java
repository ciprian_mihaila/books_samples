package jcip.sharingobjects.threadconfinment;

import java.util.Collection;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Thread confinement of local primitive and reference variables. 
 * 
 * 
 * Stack  confinement  is  a  special  case  of  thread  confinement  in  which  
 * an  object  can  only  be  reached  through  local  variables. Just as encapsulation 
 * can make it easier to preserve invariants, local variables can make it easier to confine 
 * objects  to  a  thread.  Local  variables  are  intrinsically  confined  to  the  executing  
 * thread;  they  exist  on  the  executing thread's  stack,  which  is  not  accessible  to  other  threads
 * 
 * @author Ciprian
 *
 */
public class StackConfinement {
	private Ark ark;

	public int loadTheArk(Collection<Animal> candidates) {
		SortedSet<Animal> animals;
		int numPairs = 0;
		Animal candidate = null;
		// animals confined to method, don't let them escape!
		animals = new TreeSet<Animal>(new SpeciesGenderComparator());
		animals.addAll(candidates);
		for (Animal a : animals) {
			if (candidate == null || !candidate.isPotentialMate(a))
				candidate = a;
			else {
				ark.load(new AnimalPair(candidate, a));
				++numPairs;
				candidate = null;
			}
		}
		return numPairs;
	}
}

interface Animal {
	public boolean isPotentialMate(Animal a);
}

class AnimalPair {
	public AnimalPair(Animal a, Animal b) {
	}
}

class Ark {
	public void load(AnimalPair pair) {

	}
}

class SpeciesGenderComparator implements Comparator<Animal> {

	public int compare(Animal o1, Animal o2) {
		return 0;
	}

}
