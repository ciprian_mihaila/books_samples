package jcip.sharingobjects.escape;

/**
 * implicitly allowing the this reference to escape. Don't Do this.
 * 
 * ThisEscape  publishes  the  EventListener,  it  implicitly  publishes  
 * the  enclosing  ThisEscape  instance  as  well,  because  inner  class  
 * instances  contain  a  hidden  reference  to  the  enclosing  instance
 * 
 * publishing an object from within its constructor can publish 
 * an incompletely constructed object
 * @author Ciprian
 *
 */
public class ThisEscape {
	public ThisEscape(EventSource source) {
		source.registerListener(new EventListener() {
			public void onEvent(Event e) {
				doSomething(e);
			}
		});
	}

	void doSomething(Event e) {
	}
}

interface Event {
}

interface EventListener {
	public void onEvent(Event e);
}

interface EventSource {
	public void registerListener(EventListener eventListener);
}
