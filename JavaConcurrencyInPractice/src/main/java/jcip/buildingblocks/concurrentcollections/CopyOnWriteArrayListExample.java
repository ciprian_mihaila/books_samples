package jcip.buildingblocks.concurrentcollections;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * CopyOnWriteArrayList is a concurrent replacement for a synchronized List 
 * that offers better concurrency in some common situations and eliminates 
 * the need to lock or copy the collection during iteration
 * 
 * The cop  on write collections derive their thread safety from the fact that 
 * as long as an effectively immutable object is properly published, no further 
 * synchronization is required when accessing it. They implement mutability by creating and 
 * republishing a new copy of the collection every time it is modified. 
 * 
 * Iterators for the copy on write collections retain a reference to the backing array that 
 * was current at the start of iteration, and since this will never change, they need to 
 * synchronize only briefly to ensure visibility of the array contents. As a result, 
 * multiple threads can iterate the collection without interference from one another or 
 * from threads wanting to modify the collection. The iterators returned by the copy on write
 * collections do not throw ConcurrentModificationException and return the elements  exactly as 
 * they were at the time the iterator was created, regardless of subsequent modifications. 
 * 
 * @author Ciprian
 *
 */
public class CopyOnWriteArrayListExample {
	public static void example() {
		List<String> nameList = new CopyOnWriteArrayList<String>(
				new String[] { "Peter", "Bruce", "Clark", "Barry", "Lex" });

		Iterator<String> it = nameList.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}

	}
}
