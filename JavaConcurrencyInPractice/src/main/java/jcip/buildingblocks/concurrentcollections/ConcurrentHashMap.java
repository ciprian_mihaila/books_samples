package jcip.buildingblocks.concurrentcollections;

import java.util.Iterator;
import java.util.Map;

import jcip.annotations.ThreadSafe;

/**
 * ConcurrentHashMap  is  a  concurrent  replacement  for  a  synchronized  hash based  Map
 * 
 * ConcurrentHashMap is a hash based Map like HashMap, but it uses an entirely different 
 * locking strategy that offers better concurrency and scalability. 
 * Instead of synchronizing every method on a common lock, restricting access to a single 
 * thread at a time, it uses a finer grained locking mechanism called lock striping 
 * to allow a greater degree of shared access
 * 
 * @author Ciprian
 *
 */
@ThreadSafe
public class ConcurrentHashMap {

	private Map<Integer, String> myMap = new java.util.concurrent.ConcurrentHashMap<Integer, String>();

	public void iterate() {
		Iterator<Integer> it = myMap.keySet().iterator();
		while (it.hasNext()) {
			Integer key = it.next();
			System.out.println(key);
		}
	}

}
