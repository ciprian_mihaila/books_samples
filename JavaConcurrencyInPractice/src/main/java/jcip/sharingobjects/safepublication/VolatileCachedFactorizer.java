package jcip.sharingobjects.safepublication;

import java.math.BigInteger;

import javax.servlet.GenericServlet;
import javax.servlet.Servlet;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import jcip.annotations.ThreadSafe;
import jcip.sharingobjects.immutability.OneValueCache;

/**
 * Caching the Last Result Using a Volatile Reference to an Immutable Holder Object
 * @author Ciprian
 *
 */
@ThreadSafe
public class VolatileCachedFactorizer extends GenericServlet implements Servlet {
	private static final long serialVersionUID = 1L;

	private volatile OneValueCache cache = new OneValueCache(null, null);

	public void service(ServletRequest req, ServletResponse resp) {
		BigInteger i = extractFromRequest(req);
		BigInteger[] factors = cache.getFactors(i);
		if (factors == null) {
			factors = factor(i);
			cache = new OneValueCache(i, factors);
		}
		encodeIntoResponse(resp, factors);
	}

	void encodeIntoResponse(ServletResponse resp, BigInteger[] factors) {
		System.out.println("encoded into response");
	}

	private BigInteger[] factor(BigInteger i) {
		return new BigInteger[] { i };
	}

	private BigInteger extractFromRequest(ServletRequest req) {
		return new BigInteger("1");
	}
}
